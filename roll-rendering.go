package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"text/template"
	"time"
)

type singleDie struct {
	Hero    string
	Dice    string
	Roll    string
	DivName string
}

type renderedAppend struct {
	Mtype  string `json:"type"`
	Data   string `json:"data"`
	Append string `json:"append"`
}

// diceRoll handles the websocker communication for a single dice click.
// it needs to check if it appends or creates a new row
func (cp *callerPackage) diceRoll(hero, dice string) {
	var tmpl *template.Template
	var err error
	jsInfo := "renderdicerow"
	lastTime, okay := cp.lastUpdate[hero]
	// within 3 seconds the same output row is targeted
	if okay {
		if lastTime.Add(3 * time.Second).Before(time.Now()) {
			tmpl, err = template.ParseFiles("templates/newrowdice.tmpl")
			cp.lastUpdate[hero] = time.Now()
		} else {
			tmpl, err = template.ParseFiles("templates/appenddicerow.tmpl")
			jsInfo = "renderdicerowappend"
		}
	} else {
		cp.lastUpdate[hero] = time.Now()
		tmpl, err = template.ParseFiles("templates/newrowdice.tmpl")
	}
	if err != nil {
		log.Println(err)
		return
	}
	// making the roll
	divName := fmt.Sprint(cp.lastUpdate[hero].Unix())
	die := &singleDie{
		Hero:    hero,
		Dice:    dice,
		Roll:    strconv.Itoa(rand.Intn(6) + 1),
		DivName: divName,
	}

	var buf bytes.Buffer
	err = tmpl.Execute(&buf, die)
	if err != nil {
		log.Println(err)
		return
	}
	clientjson := &renderedAppend{Mtype: jsInfo, Data: base64.StdEncoding.EncodeToString(buf.Bytes()), Append: divName}
	clientblob, _ := json.Marshal(clientjson)
	cp.hub.broadcast <- clientblob
}

// renderAttacks handles the hub communication for the hero attack roll.
func (cp *callerPackage) renderAttackRolls(hero string) {
	clientjson := &rendered{Mtype: "renderweapon", Data: heroAttackTemplate(cp, hero)}
	clientblob, _ := json.Marshal(clientjson)
	cp.hub.broadcast <- clientblob
}

// heroAttackTemplate composes diceoutput, base64 the html and parses it back.
func heroAttackTemplate(cp *callerPackage, hero string) string {
	tmpl, err := template.ParseFiles("templates/heroattack.tmpl")
	if err != nil {
		log.Println(err)
	}
	var buf bytes.Buffer
	err = tmpl.Execute(&buf, heroAttack(cp, hero))
	if err != nil {
		log.Println(err)
	}
	return base64.StdEncoding.EncodeToString(buf.Bytes())
}
