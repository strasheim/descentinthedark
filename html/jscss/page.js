// Both used for typaheads 
var herolist = []; 
var skilllist = [];

var loc = window.location, new_uri;
if (loc.protocol === "https:") {
  new_uri = "wss://" + loc.host + loc.pathname + "ws";
} else {
  new_uri = "ws://" +loc.host + loc.pathname + "ws";
}

var ws = initWS();
function initWS() {
  var socket = new WebSocket(new_uri), container = $("#wscontainer")
  socket.onopen = function() {
    triggerchange("Overlord","reload")
  };
  socket.onmessage = function(e) {
    console.log(truncateString(e.data,200))
    var ks = e.data.split("\n");
    ks.forEach(function(entry) {
      var msg = JSON.parse(entry)
      switch (msg.type) {
        case "renderdicerow":  
          $("#wscontainer").prepend(atob(msg.data)).children(':first').delay(300000).fadeOut(2000);
        break;

        case "renderdicerowappend":
          $("#"+msg.append).append(atob(msg.data).trim())
        break;

        case "renderweapon":
          $("#wscontainer").prepend(atob(msg.data)).children(':first').delay(300000).fadeOut(2000);
        break;

        case "rendermarket":
          $('#Shopitems').html(atob(msg.data));
        break;

        case "renderedinventory":
          $('#myitems').html(atob(msg.data));
        break;

        case "renderherolist":
          $('#wscontainer2').html(atob(msg.data));
        break;
        }
      });
    }
    socket.onclose = function() {
      setTimeout( function() { ws = initWS() }, 3000);
    }
    return socket;
  }

$(document).on("click", "#youtube", function(e) {
  $('#movearound').html(
    "<div class='embed-responsive embed-responsive-16by9'> <iframe class='embed-responsive-item' " + 
    "  src='https://www.youtube.com/embed/live_stream?channel=UCtetobiruSmC-XJAFBUQDBg' frameborder='0' " +
    "  ></iframe> </div>");
});

/*
$(document).on("click", "#youtube", function(e) {
  $('#movearound').html(
    "<div class='embed-responsive embed-responsive-16by9'> <iframe class='embed-responsive-item'  src='//youtube.com/embed/" +
    $('#ylink').val() + "'></iframe> </div>");
});
*/

// Simple function, on simple buttons
$(document).on("click", "#gold", function(e) { draw("gold") });
$(document).on("click", "#silver", function(e) { draw("silver") });
$(document).on("click", "#copper", function(e) { draw("copper") });
$(document).on("click", "#fighting", function(e) { draw("fighting") });
$(document).on("click", "#subterfuge", function(e) { draw("subterfuge") });
$(document).on("click", "#wizardry", function(e) { draw("wizardry") });
$('.roll').on('click', function(e) { roll($(this).attr("value"))});
$('#healthup').on('click', function(e)    { triggerchange(localStorage.hero, "healthup") });
$('#healthdown').on('click', function(e)  { triggerchange(localStorage.hero, "healthdown") });
$('#fatigueup').on('click', function(e)   { triggerchange(localStorage.hero, "fatigueup") });
$('#fatiguedown').on('click', function(e) { triggerchange(localStorage.hero, "fatiguedown") });
$('#reloadstats').on('click', function(e) { triggerchange("Overlord","reload") });
$(document).on("click", "#removehero", function(e) { 
  triggerchange($('#heroinput').val(),"deselecthero");
  localStorage.hero="undefined";
});
$(document).on("click", "#selecthero", function(e) { 
  triggerchange($('#heroinput').val(),"selecthero"); 
  localStorage.hero=$('#heroinput').val();
  var urlname = localStorage.hero.replace(/ /g, "-").toLowerCase()
  $('#heroinfoimage').html('<img src="images/heros/' + urlname + '.png" width="845px">');
  $("#herohint").text(localStorage.hero);
});

$(document).on("click", ".inventorylist", function(e) {
  target = $(this).attr("heroto");
  action = $(this).attr("action");
  owner = $(this).attr("owner");
  stuff  = $(this).attr("stuff");

  if ( action == "buy" || action == "sell" || action == "equip" || action == "unequip" ) {
    target = localStorage.hero;
  }
  console.log(owner + "'s: " + stuff + " to " + action +": " + target)  	
  $.ajax({
    url: "changeinventory",
    type: "GET",
    data: "owner="+owner+"&item="+stuff+"&action="+action+"&target="+target,
    success: function(response) {},
    error: function(response) {
      alert('Changing Inventory failed');
    },
  });
});

// This should be called SET heroskill not Get heroskill
$(document).on("click", "#getheroskill1", function(e) {
  hero = localStorage.hero; 
  skill = $('#heroskill1').val();  
  $.ajax({
    url: "setheroskill",
    type: "GET",
    data: "hero="+hero+"&skill="+skill,
    success: function(response) {},
    error: function(response) {
      alert('Changing HeroSkill 1 failed');
    },
  });
});

// used for all small changes to the game state
function triggerchange(hero, type) {
console.log(hero,"triggered a change: " + type)
$.ajax({
    url: "triggerchange",
    type: "GET",
    data: "hero="+hero+"&type="+type,
    success: function(response) {},
    error: function(response) {
      alert('ServerCalling ' + url + ' failed');
    },
  });
};

$(document).on("click", ".itemlist", function(e) {
  hero = $(this).attr("hero");
  type = $(this).attr("action");
  console.log("itemlist "+hero+" "+type)
  switch (type) {
    case "healthplus":
    case "healthminus":
    case "healthbuy":
    case "fatigueplus":
    case "fatigueminus":
    case "fatiguebuy":
    case "goldp50":
    case "goldp100":
    case "goldm25":
    case "goldm100":
    case "healthup":
    case "healthdown":
    case "fatigueup":
    case "fatiguedown":
      triggerchange(hero,type)
    break;
    case "buycopper":
      draw("copper")
      triggerchange(hero,"goldm250")
    break;
    case "buysilver":
      draw("silver")
      triggerchange(hero,"goldm500")
    break;
    case "buygold":
      draw("gold")
      triggerchange(hero,"goldm750")
    break;
  }
});

// Drawing a skill or treasure
function draw(what) {
  $.ajax({
    url: "draw",
    type: "GET",
    data: "draw=" + what+"&hero="+localStorage.hero,
    success: function(response) {},
    error: function(response) {
      alert('Draw failed');
    },
  });
};

// the generic dice roll 
function roll(color) {
  $.ajax({
    url: "dice",
    type: "GET",
    data: "roll=" + color+"&hero="+localStorage.hero,
    success: function(response) {},
    error: function(response) {
      alert('Dice roll failed');
    },
  });
};

// the attackroll only needs to know the hero
$(document).on("click", '.weapon', function(e) {
  var attackhero = $(this).attr("id");
  $.ajax({
    url: "attackroll",
    type: "GET",
    data: "hero="+attackhero,
    success: function(response) {},
    error: function(response) {
      alert('Attack Roll: Action failed');
    },
  });
});

function getskilllist() {
   $.ajax({
      url: "getskilllist",
      type: "GET",
      dataType : 'json',
      success: function(response) {
        skilllist = response.skills;
        console.log(skilllist)
      },
      error: function(response) {
        alert('failed to load the skill'+response);
      },
    });
}

function getherolist() {
   $.ajax({
      url: "getherolist",
      type: "GET",
      dataType : 'json',
      success: function(response) {
        herolist = response.names;
        console.log(herolist)
      },
      error: function(response) {
        alert('failed to load the heros'+response);
      },
    });
}

// to make the console logging of server returns less loud
function truncateString(str, num) {
  if (str.length <= num) {
    return str
  }
  return str.slice(0, num) + '...'
}

// End of File :) --- load on page completely loaded. This fills the dynamic divs with content
// this called even after the iframes and images are loaded
$(document).ready(function () {
  getherolist();
  getskilllist();
});
$( window ).on( "load", function() {
  $('#myHeroNameModal').modal('show');
  console.log("Serving the honorable hero: "+ localStorage.hero)
  if (localStorage.hero !== undefined) {
    console.log("hero is already known")
    hero = localStorage.hero 
    var urlname = hero.replace(/ /g, "-").toLowerCase()
    $('#heroinfoimage').html('<img src="images/heros/' + urlname + '.png" width="845px">');
    $("#herohint").text(hero);
  }
  // setting value in the hero modal -- undefined on blank/new browser
  document.getElementById('heroinput').value = localStorage.hero;
  //selecthero(localStorage.hero)

  setTimeout(function() {
     $('#scrollable-dropdown-menu .typeahead').typeahead(null,{
     name: 'herolist',
     limit: '10',
     source: substringMatcher(herolist)
     });
  }, 500);
  setTimeout(function() {
     $('#scrollable-dropdown-menu-skill .typeahead').typeahead(null,{
     name: 'heroskills',
     limit: '16',
     source: substringMatcher(skilllist)
     });
  }, 1000);
});
