package main

import "strings"

// the single character dice names to be real HTML names.
func dice2html(s, size string) string {
	s = strings.ReplaceAll(s, "R", "<img src='images/red2.png' width='"+size+"'>")
	s = strings.ReplaceAll(s, "W", "<img src='images/white2.png' width='"+size+"'>")
	s = strings.ReplaceAll(s, "B", "<img src='images/blue4.png' width='"+size+"'>")
	s = strings.ReplaceAll(s, "G", "<img src='images/green1.png' width='"+size+"'>")
	s = strings.ReplaceAll(s, "Y", "<img src='images/yellow2.png' width='"+size+"'>")
	s = strings.ReplaceAll(s, "S", "<img src='images/black4.png' width='"+size+"'>")
	s = strings.ReplaceAll(s, "X", "<img src='images/silver4.png' width='"+size+"''>")
	s = strings.ReplaceAll(s, "Z", "<img src='images/gold4.png' width='"+size+"'>")
	return s
}

// pipe2newLines does the item info conversion to be html ready.
func pipe2newLines(s, size string) string {
	s = strings.ReplaceAll(s, "|", "</br>")
	s = strings.ReplaceAll(s, "*", "<img src='images/energie.png' width='"+size+"'>")
	return s
}

// hands2icons replaces the hands by an icon if needed
// for rendering.
func hands2icons(hands string) string {
	switch hands {
	case "2":
		return "<br> <img src='images/bothhands.png' width='36' />"
	case "1":
		return "<br> <img src='images/linkehand.png' width='18' />"
	default:
		return ""
	}
}
