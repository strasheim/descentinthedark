package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"log"
	"sort"
	"text/template"
)

// updateMarket displays the market with all its data it does the complete
// rendering
func (cp *callerPackage) updateMarket() {
	allMarketItems(cp.items)
	clientjson := &rendered{Mtype: "rendermarket", Data: marketTemplate(cp.items, herosInUse(cp.heros))}
	clientblob, _ := json.Marshal(clientjson)
	cp.hub.broadcast <- clientblob
	log.Println("market rendered")
}

// marketTemplate prints out rendered HTML for complete market modal
func marketTemplate(i allItems, h []string) string {
	tmpl, _ := template.ParseFiles("templates/market-modal.tmpl")
	var buf bytes.Buffer
	err := tmpl.Execute(&buf, sortTheMarket(i, h))
	if err != nil {
		return err.Error()
	}
	return base64.StdEncoding.EncodeToString(buf.Bytes())
}

// sort the Market, does sort the items in the list by name and class
// items not for print are also removed
func sortTheMarket(a allItems, h []string) allItems {
	var market, copper, silver, gold allItems
	for _, item := range a {
		if item.Owner != "market" {
			continue
		}
		if item.Itemtype == "Cache" {
			continue
		}
		item.Hands = hands2icons(item.Hands)
		item.Attack = dice2html(item.Attack, "18")
		item.Effect = pipe2newLines(item.Effect, "13")
		item.Details = h
		switch item.Rating {
		case "shop":
			market = append(market, item)
		case "copper":
			copper = append(copper, item)
		case "silver":
			silver = append(silver, item)
		case "gold":
			gold = append(gold, item)
		default:
			continue
		}
	}
	sort.Slice(market, func(i, j int) bool {
		return market[i].Name < market[j].Name
	})
	sort.Slice(copper, func(i, j int) bool {
		return copper[i].Name < copper[j].Name
	})
	sort.Slice(silver, func(i, j int) bool {
		return silver[i].Name < silver[j].Name
	})
	sort.Slice(gold, func(i, j int) bool {
		return gold[i].Name < gold[j].Name
	})
	market = append(market, copper...)
	market = append(market, silver...)
	market = append(market, gold...)
	return market
}
