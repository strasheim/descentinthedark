// nolint - reads in the struct
package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
)

type allItems []struct {
	Rating   string `json:"rating"`
	Owner    string `json:"owner"`
	Name     string `json:"name"`
	Cost     int    `json:"cost"`
	Itemtype string `json:"itemtype"`
	Hands    string `json:"hands"`
	Equipped bool   `json:"equipped"`
	Attack   string `json:"attack"`
	Origin   string `json:"origin"`
	Effect   string `json:"effect"`
	Index    int    `json:"index"`
	Details  []string
}

// drawitem is really draw treasure, item is picked from the stack
// and added to the inventory of the heros.
func drawitem(rating string, hero string, items allItems) string {
	var available []int
	for i, v := range items {
		if v.Rating == rating && v.Owner == "market" {
			available = append(available, i)
		}
	}
	if len(available) >= 1 {
		n := rand.Int() % len(available)
		n = available[n]
		items[n].Owner = hero
		if items[n].Itemtype == "Cache" {
			return items[n].Effect
		}
		return items[n].Name
	} else {
		return "Out of Treasures"
	}
}

// changeinventory changes the state of the of ownership on any item of the global
// item list.
func changeinventory(world *callerPackage, owner, item, action, target string) {
	items := world.items
	index, err := strconv.Atoi(item)
	if err != nil {
		log.Println("failed to read item inded", err)
		return
	}
	heroIndex := -1
	for hIndex := range world.heros {
		if world.heros[hIndex].Name == target {
			heroIndex = hIndex
		}
	}
	log.Println("item change request from", owner, "for", item, items[index].Name, "action", action, "target", target)
	if heroIndex == -1 {
		log.Println("not changing item as hero can't be found")
		return
	}
	switch action {
	case "equip":
		items[index].Owner = target
		items[index].Equipped = true
		switch {
		case strings.Contains(items[index].Itemtype, "Weapon"):
			world.heros[heroIndex].ActiveWeapon = items[index].Name
		case strings.Contains(items[index].Itemtype, "Armor"):
			world.heros[heroIndex].ActiveArmor = items[index].Name
		case strings.Contains(items[index].Itemtype, "Shield"):
			world.heros[heroIndex].ActiveShield = items[index].Name
		}
	case "unequip", "move":
		items[index].Owner = target
		items[index].Equipped = false
		switch {
		case strings.Contains(items[index].Itemtype, "Weapon"):
			world.heros[heroIndex].ActiveWeapon = ""
		case strings.Contains(items[index].Itemtype, "Armor"):
			world.heros[heroIndex].ActiveArmor = ""
		case strings.Contains(items[index].Itemtype, "Shield"):
			world.heros[heroIndex].ActiveShield = ""
		}
	case "drop":
		items[index].Owner = "market"
		items[index].Equipped = false
		switch {
		case strings.Contains(items[index].Itemtype, "Weapon"):
			world.heros[heroIndex].ActiveWeapon = ""
		case strings.Contains(items[index].Itemtype, "Armor"):
			world.heros[heroIndex].ActiveArmor = ""
		case strings.Contains(items[index].Itemtype, "Shield"):
			world.heros[heroIndex].ActiveShield = ""
		}
	case "buy":
		items[index].Owner = target
		items[index].Equipped = false
		world.heros[heroIndex].Gold -= items[index].Cost
	case "sell":
		if items[index].Rating == "relics" {
			log.Println("Relics can't be sold")
			return
		}
		switch {
		case strings.Contains(items[index].Itemtype, "Weapon"):
			world.heros[heroIndex].ActiveWeapon = ""
		case strings.Contains(items[index].Itemtype, "Armor"):
			world.heros[heroIndex].ActiveArmor = ""
		case strings.Contains(items[index].Itemtype, "Shield"):
			world.heros[heroIndex].ActiveShield = ""
		}
		items[index].Owner = "market"
		items[index].Equipped = false
		world.heros[heroIndex].Gold += ((items[index].Cost / 25) + 1) / 2 * 25
	}
	// update the world
	go world.updateInventory()
	go world.updateMarket()
	go world.renderherolist()
}

// allMarketItems filters down the list of items to the subset
// of those still on the market
func allMarketItems(items allItems) allItems {
	var available []int
	var inventory allItems
	for i, v := range items {
		if v.Owner == market {
			available = append(available, i)
		}
	}
	for _, n := range available {
		inventory = append(inventory, items[n])
	}
	return inventory
}

// readjson slurps in the items file from disk
func readjson(file string) allItems {
	var data, copy allItems
	content, err := os.ReadFile(file)
	if err != nil {
		log.Panic(err, file+" not found\n")
	}
	err = json.Unmarshal(content, &data)
	if err != nil {
		log.Panic(err)
	}
	for i, d := range data {
		copy = append(copy, d)
		copy[i].Index = i
	}
	return copy
}
