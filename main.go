// descentinthedark webserver - mostly a info dealer und randomnumber push out
package main

import (
	"crypto/tls"
	"encoding/json"
	"log"
	"net/http"
	"time"
)

// default struct for all websocket answers
// folling TLV, without the L as not needed in our string like world.
type rendered struct {
	Mtype string `json:"type"`
	Data  string `json:"data"`
}

type callerPackage struct {
	hub        *Hub
	items      allItems
	heros      allHeros
	skills     allSkills
	lastUpdate map[string]time.Time
}

func main() {
	var cp callerPackage
	cp.items = readjson("items.json")
	cp.skills = readskilljson("skills.json")
	cp.heros = readherojson("heros.json")
	cp.hub = newHub()
	cp.lastUpdate = make(map[string]time.Time)
	go cp.hub.run()

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(cp.hub, w, r)
	})

	http.HandleFunc("/attackroll", cp.attackroll)       // ws
	http.HandleFunc("/dice", cp.rolldice)               // ws
	http.HandleFunc("/draw", cp.treasureDraw)           // ws
	http.HandleFunc("/getskilllist", cp.getskilllist)   // ajax
	http.HandleFunc("/getherolist", cp.getherolist)     // ajax
	http.HandleFunc("/glossar/glossardata", cp.glossar) // different page ajax
	http.HandleFunc("/setheroskill", cp.setheroskill)   // ws
	http.HandleFunc("/triggerchange", cp.triggerchange) // ws

	// signature does not match, generic update function all of which are using
	// the websocket to get back the data
	http.HandleFunc("/heroinfo", func(w http.ResponseWriter, r *http.Request) {
		cp.renderherolist()
	})
	http.HandleFunc("/updateinventory", func(w http.ResponseWriter, r *http.Request) {
		cp.updateInventory()
	})
	http.HandleFunc("/openmarket", func(w http.ResponseWriter, r *http.Request) {
		cp.updateMarket()
	})

	// Market Changes or Heros using Equip trigger this function
	http.HandleFunc("/changeinventory", func(w http.ResponseWriter, r *http.Request) {
		changeinventory(&cp, r.FormValue("owner"), r.FormValue("item"), r.FormValue("action"), r.FormValue("target"))
	})

	http.Handle("/", http.FileServer(http.Dir("./html")))
	http.Handle("/glossar/", http.StripPrefix("/glossar/", http.FileServer(http.Dir("./glossar"))))
	tlsconfig, err := runTLS()
	if err != nil {
		log.Println("Listening...")
		must(http.ListenAndServe(":3000", nil))
	} else {
		server := http.Server{
			Addr:         ":3000",
			TLSConfig:    tlsconfig,
			TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
		}
		log.Println("TLS Listening...")
		must(server.ListenAndServeTLS("", ""))
	}
}

// rolldice makes a single diceroll and pushed that info to the websocket
func (cp *callerPackage) rolldice(w http.ResponseWriter, r *http.Request) {
	hero := r.FormValue("hero")
	color := r.FormValue("roll")
	if validHero(hero, cp.heros) {
		cp.diceRoll(hero, color)
	}
}

// attackroll looks up the heros weapon compiles the attack and adds
// randomniss to the output.
func (cp *callerPackage) attackroll(w http.ResponseWriter, r *http.Request) {
	hero := r.FormValue("hero")
	if validHero(hero, cp.heros) {
		cp.renderAttackRolls(hero)
	}
}

// getskilllist updates the list of skills to the clients, pure Ajax as its
// one time output which does not have any changes, typeahead data
func (cp *callerPackage) getskilllist(w http.ResponseWriter, r *http.Request) {
	var skillNames []string
	for _, skill := range cp.skills {
		skillNames = append(skillNames, skill.Skill)
	}
	pushSkills := struct {
		S []string `json:"skills"`
	}{
		S: skillNames,
	}
	clientblob, err := json.Marshal(pushSkills)
	if err != nil {
		log.Println("failed to marshal skills:", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	}
	w.Write(clientblob)
	log.Println("Skills ajaxed to client")
}

// getherolist updates the list of heros the clients, pure Ajax as its
// one time output which does not have any changes, typeahead data
func (cp *callerPackage) getherolist(w http.ResponseWriter, r *http.Request) {
	var names []string
	for _, hero := range cp.heros {
		names = append(names, hero.Name)
	}
	heronames := struct {
		N []string `json:"names"`
	}{
		N: names,
	}
	clientblob, err := json.Marshal(heronames)
	if err != nil {
		log.Println("failed to marshal heros:", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	}
	w.Write(clientblob)
	log.Println("Heros ajaxed to client")
}

// cause we want to learn what is breaking and where, also we appear to recover a lot
func must(err error) {
	if err != nil {
		panic(err)
	}
}
