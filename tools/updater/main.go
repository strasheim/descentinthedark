package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

var url = "http://localhost:3000/importer"

type urlData struct {
	Object    string    `json:"object"`
	SkillData allSkills `json:"skills"`
	ItemData  allItems  `json:"items"`
	HeroData  allHeros  `json:"heros"`
}

func main() {

	if len(os.Args) != 3 {
		fmt.Println("Usage: updater [savedirectory] [serverurl:port]")
		return
	}

	url = "https://" + os.Args[2] + "/importer"
	directory := os.Args[1] + "/"

	var skills allSkills
	data, err := ioutil.ReadFile(directory + "skills.json")
	errexit(err)
	json.Unmarshal(data, &skills)
	uploader(json.Marshal(urlData{Object: "skills", SkillData: skills}))

	var items allItems
	data, err = ioutil.ReadFile(directory + "items.json")
	errexit(err)
	json.Unmarshal(data, &items)
	uploader(json.Marshal(urlData{Object: "items", ItemData: items}))

	var heros allHeros
	data, err = ioutil.ReadFile(directory + "heros.json")
	errexit(err)
	json.Unmarshal(data, &heros)
	uploader(json.Marshal(urlData{Object: "heros", HeroData: heros}))

}

func uploader(jsonStr []byte, err error) {
	errexit(err)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	_, err = client.Do(req)
	errexit(err)
}

func errexit(e error) {
	if e != nil {
		fmt.Println(e)
		os.Exit(2)
	}
}
