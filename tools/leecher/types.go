package main

type message struct {
	Mtype      string      `json:"type"`
	Name       string      `json:"name"`
	Items      allItems    `json:"items"`
	Heros      allHeros    `json:"heros"`
	Skills     allSkills   `json:"skills"`
	Health     interface{} `json:"health"`
	Fatigue    interface{} `json:"fatigue"`
	Gold       interface{} `json:"gold"`
	HeroWeapon string      `json:"heroweapon"`
	Heroinfo   string      `json:"heroinfo"`
	Color      string      `json:"color"`
	Roll       string      `json:"roll"`
	Result     string      `json:"result"`
	Ctype      string      `json:"chest"`
	Item       string      `json:"item"`
	Hero       string      `json:"hero"`
	Melee      int         `json:"melee"`
	Ranged     int         `json:"ranged"`
	Magic      int         `json:"magic"`
	Time       string      `json:"time"`
}

type allItems []struct {
	Rating   string `json:"rating"`
	Owner    string `json:"owner"`
	Name     string `json:"name"`
	Cost     int    `json:"cost"`
	Itemtype string `json:"itemtype"`
	Hands    string `json:"hands"`
	Equipped int    `json:"equipped"`
	Attack   string `json:"attack"`
	Effect   string `json:"effect"`
}

type allHeros []struct {
	Name       string `json:"Name"`
	Wounds     int    `json:"Wounds"`
	Fatigue    int    `json:"Fatigue"`
	Armor      int    `json:"Armor"`
	Speed      int    `json:"Speed"`
	Melee      int    `json:"Melee"`
	Ranged     int    `json:"Ranged"`
	Magic      int    `json:"Magic"`
	Fighting   int    `json:"Fighting"`
	Subterfuge int    `json:"Subterfuge"`
	Wizardry   int    `json:"Wizardry"`
	Conquest   int    `json:"Conquest"`
	Silver     int    `json:"Silver"`
	Gold       int    `json:"Gold"`
	Experience int    `json:"Experience"`
	Ability    string `json:"Ability"`
	Owner      string `json:"Owner"`
}

type allSkills []struct {
	Skill        string `json:"Skill"`
	Details      string `json:"Details"`
	Skilltype    string `json:"Skilltype"`
	Owner        string `json:"Owner"`
	RoadToLegend string `json:"Road to Legend"`
}

type singleHero struct {
	Mtype   string  `json:"type"`
	Hero    string  `json:"hero"`
	Health  string  `json:"health"`
	Fatigue string  `json:"fatigue"`
	Gold    float64 `json:"gold"`
	Name    string  `json:"name"`
}

type heroupdate struct {
	Health  string `json:"health"`
	Fatigue string `json:"fatigue"`
}

type statsRolls struct {
	Hero  string `json:"hero"`
	Color string `json:"color"`
	Roll  string `json:"roll"`
}
