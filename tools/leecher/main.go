package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/websocket"
)

var rolls []statsRolls
var old time.Time
var savegame string
var savegameCounter int
var URL = "ws://localhost:3000/ws"
var dialer *websocket.Dialer
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func wsprinter(ws *websocket.Conn) {
	var msg message
	for {
		err := ws.ReadJSON(&msg)
		if err != nil {
			log.Println(err)
			return
		}

		// time checking - have 5 minute passed since the last time I was here?
		passed := time.Now().Sub(old)
		if passed.Minutes() >= 5 {
			savegameCounter++
			if savegameCounter == 1 {
				savegameCounter = 0
			}
			savegame = "savegame" + strconv.Itoa(savegameCounter) + "/"
			os.Mkdir(savegame, 0777)
			log.Println(savegame, "created")
			old = time.Now()
			x := make(map[string][]int)
			for _, v := range rolls {
				value, _ := strconv.Atoi(v.Roll)
				x[v.Hero] = append(x[v.Hero], value)
			}
			for i, v := range x {
				var total float64 = 0
				for _, value := range v {
					total += float64(value)
				}
				heroname, _ := url.PathUnescape(i)
				fmt.Printf("An Average of %3.2f for %s rolls\n", total/float64(len(v)), heroname)
			}

		}
		// Overview of the drama at hand: <msg.Mtype>
		// # append - normal dice roles
		// # draw - skill or chest draw result
		// # weapon - attack with a weapon, dice mix
		// # otherheros - notification to other to trigger `update`
		// # update - each player sends out info about himself ( life and such )
		// ---- WS call from the browser, breaks a few things as it's using int stead of strings
		// # itemupdate - Gold, Potions ..
		// --- BS for having 3 given partial updates which could be sorted on the client side ---
		// # inventory - items which are non-market
		// # openmarket - items which are owned by market
		// # equipeditems - per hero view of his items which are equipped
		// # openmarket and inventory now have a hook to trigger fullinventory , which has all items
		// --------------------------------------------------------------------------------------
		// # herolist - contains all heros with their values
		// # allskill - contains all skills with owner and such

		// fmt.Println(msg.Mtype)
		switch msg.Mtype {
		case "append":
			roll := statsRolls{msg.Name, msg.Color, msg.Roll}
			rolls = append(rolls, roll)
		case "weapon":
			singles := strings.Split(msg.Result, "")
			color := ""
			for i, v := range singles {
				if i%2 == 0 {
					switch v {
					case "R":
						color = "red"
					case "B":
						color = "blue"
					case "W":
						color = "white"
					case "Y":
						color = "yellow"
					case "G":
						color = "green"
					case "S":
						color = "black"
					case "X":
						color = "silver"
					case "Z":
						color = "gold"
					}
				} else {
					roll := statsRolls{msg.Name, color, v}
					rolls = append(rolls, roll)
				}
			}

		case "update":
			printme := heroupdate{
				Health:  msg.Health.(string),
				Fatigue: msg.Fatigue.(string),
			}
			file, _ := json.MarshalIndent(printme, "", "  ")
			data := []byte(file)
			err = ioutil.WriteFile(savegame+msg.Name+".json", data, 0644)
			if err != nil {
				log.Println("Creating local for", msg.Name, "copy failed")
			}
		case "itemUpdate":
			health, _ := msg.Health.(string)
			fatigue, _ := msg.Fatigue.(string)
			gold, _ := msg.Gold.(float64)
			printme := singleHero{
				Mtype:   "itemUpdate",
				Hero:    msg.Hero,
				Health:  health,
				Fatigue: fatigue,
				Gold:    gold,
				Name:    "undefined",
			}
			file, _ := json.MarshalIndent(printme, "", "  ")
			data := []byte(file)
			err = ioutil.WriteFile(savegame+msg.Hero+".json", data, 0644)
			if err != nil {
				log.Println("Creating local for", msg.Hero, "copy failed")
			}
		case "fullinventory": // this is not the complete list, just items picked by heros  |  openmarket + inventory is all
			printme := msg.Items
			file, _ := json.MarshalIndent(printme, "", "  ")
			data := []byte(file)
			err = ioutil.WriteFile(savegame+"items.json", data, 0644)
			if err != nil {
				log.Println("Creating local item copy failed")
			}
		case "inventory": // this is not the complete list, just items picked by heros  |  openmarket + inventory is all
			printme := msg.Items
			file, _ := json.MarshalIndent(printme, "", "  ")
			data := []byte(file)
			err = ioutil.WriteFile(savegame+"items1.json", data, 0644)
			if err != nil {
				log.Println("Creating local item1 copy failed")
			}
		case "openmarket": // this is not the complete list, just items picked by heros  |  openmarket + inventory is all
			printme := msg.Items
			file, _ := json.MarshalIndent(printme, "", "  ")
			data := []byte(file)
			err = ioutil.WriteFile(savegame+"items2.json", data, 0644)
			if err != nil {
				log.Println("Creating local item2 copy failed")
			}
		case "herolist":
			printme := msg.Heros
			file, _ := json.MarshalIndent(printme, "", "  ")
			data := []byte(file)
			err = ioutil.WriteFile(savegame+"heros.json", data, 0644)
			if err != nil {
				log.Println("Creating local heros copy failed")
			}
		case "allskill":
			printme := msg.Skills
			file, _ := json.MarshalIndent(printme, "", "  ")
			data := []byte(file)
			err = ioutil.WriteFile(savegame+"skills.json", data, 0644)
			if err != nil {
				log.Println("Creating local skills copy failed")
			}
		}

	}
}

func init() {
	old = time.Now()
	savegameCounter = 0
	savegame = "savegame0/"
	os.Mkdir(savegame, 0777)
}

func main() {
	if len(os.Args) > 1 {
		URL = "wss://" + os.Args[1] + "/ws"
		fmt.Println(URL)
	}

	for {
		conn, _, err := dialer.Dial(URL, nil)
		if err != nil {
			log.Println(err)
			time.Sleep(time.Second * 5)
		} else {
			// single leecher - reads updates and writes to disk
			wsprinter(conn)
		}
	}
}
