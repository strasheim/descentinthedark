package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"log"
	"net/http"
	"text/template"
)

type treasureDraw struct {
	Hero     string
	Treasure string
	Link     string
	DivName  string
	CType    string
}

// treasureDraw handles the websocker communication for a single random treasure.
func (cp *callerPackage) treasureDraw(w http.ResponseWriter, r *http.Request) {
	var item string
	hero := r.FormValue("hero")
	if !validHero(hero, cp.heros) {
		log.Println("hero was not found among our frieds:", hero)
		return
	}
	ctype := r.FormValue("draw")
	switch ctype {
	case "copper":
		item = drawitem("copper", hero, cp.items)
	case "silver":
		item = drawitem("silver", hero, cp.items)
	case "gold":
		item = drawitem("gold", hero, cp.items)
	case "fighting":
		item = drawskill("Fighting", hero, cp.skills)
	case "subterfuge":
		item = drawskill("Subterfuge", hero, cp.skills)
	case "wizardry":
		item = drawskill("Wizardry", hero, cp.skills)
	default:
		log.Println(hero, "tried to draw:", ctype, "which does not exit")
		return
	}

	tmpl, err := template.ParseFiles("templates/treasurerow.tmpl")
	if err != nil {
		log.Println(err)
	}
	t := &treasureDraw{
		Hero:     hero,
		Treasure: item,
		CType:    ctype,
		Link:     "./glossar/#" + item,
		DivName:  hero,
	}

	var buf bytes.Buffer
	err = tmpl.Execute(&buf, t)
	if err != nil {
		log.Println(err)
		return
	}
	// Update the chat
	clientjson := &rendered{Mtype: "renderdicerow", Data: base64.StdEncoding.EncodeToString(buf.Bytes())}
	clientblob, _ := json.Marshal(clientjson)
	cp.hub.broadcast <- clientblob

	// Update the world
	go cp.updateInventory()
	go cp.updateMarket()
	go cp.renderherolist()
}
