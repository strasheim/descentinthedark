// nolint -- there many minor things
package main

import (
	"bytes"
	"net/http"
	"net/url"
	"strings"
	"text/template"
)

type item struct {
	Rating   string `json:"rating"`
	Owner    string `json:"owner"`
	Name     string `json:"name"`
	Cost     int    `json:"cost"`
	Itemtype string `json:"itemtype"`
	Hands    string `json:"hands"`
	Equipped bool   `json:"equipped"`
	Attack   string `json:"attack"`
	Origin   string `json:"origin"`
	Effect   string `json:"effect"`
	Index    int    `json:"index"`
	Details  []string
}

type skill struct {
	Skill        string `json:"Skill"`
	Details      string `json:"Details"`
	Skilltype    string `json:"Skilltype"`
	Owner        string `json:"Owner"`
	Origin       string `json:"origin"`
	RoadToLegend string `json:"Road to Legend"`
}

// glossar searches all data to find a match and present the content
// in a viewable way, this part is only called via AJAX never via WS.
func (cp *callerPackage) glossar(w http.ResponseWriter, r *http.Request) {
	myUrl, err := url.Parse(r.RequestURI)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}
	params, err := url.ParseQuery(myUrl.RawQuery)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}
	infoRequest := strings.ReplaceAll(params.Get("info"), "-", " ")

	w.Write(cp.findDetails(infoRequest))
}

// findDetails searches all skills,heros,items for a match and prints
// details in pure htlm after rendering.
func (cp *callerPackage) findDetails(req string) []byte {
	for _, item := range cp.items {
		if strings.ToLower(item.Name) == strings.ToLower(req) {
			item.Attack = dice2html(item.Attack, "35")
			item.Effect = pipe2newLines(item.Effect, "13")
			return itemTemplate(item)
		}
	}

	for _, skill := range cp.skills {
		if strings.ToLower(skill.Skill) == strings.ToLower(req) {
			return skillTemplate(skill)
		}
	}

	for _, hero := range cp.heros {
		if strings.ToLower(hero.Name) == strings.ToLower(req) {

		}
	}

	return []byte("no matching Hero, Item or Skill found")
}

// itemTemplate prints out rendered HTML for a given item.
func itemTemplate(i item) []byte {
	tmpl, _ := template.ParseFiles("templates/glossar-item.tmpl")
	var buf bytes.Buffer
	err := tmpl.Execute(&buf, i)
	if err != nil {
		return []byte(err.Error())
	}
	return buf.Bytes()
}

// itemTemplate prints out rendered HTML for a given item.
func skillTemplate(s skill) []byte {
	tmpl, _ := template.ParseFiles("templates/glossar-skill.tmpl")
	var buf bytes.Buffer
	err := tmpl.Execute(&buf, s)
	if err != nil {
		return []byte(err.Error())
	}
	return buf.Bytes()
}
