## Creating a webserver that helps running the boardgame

Non Tech information can be found in the [Wiki](https://gitlab.com/strasheim/descentinthedark/wikis/home)

### Build your docker image and run it 
```
Get the dockerfile
docker build --no-cache -t intothedark .
docker run -it --rm -p 8080:3000 intothedark
```

### Use a prebuild docker image 
```
docker login registry.gitlab.com
docker pull registry.gitlab.com/strasheim/descentinthedark
docker run -it --rm -p 8080:3000 descentinthedark
```

### Build without docker 
```
go get -u gitlab.com/strasheim/descentinthedark
cd $GOPATH/src/gitlab.com/strasheim/descentinthedark 
go build 
./descentinthedark
```

### You need a server per game 
Currently the server is there to handle 1 game only. It keeps the state of the treasures in memory. Stopping a server will cause that information to be lost. 
There is a tools directory which has a way around losing the content and saving it off. 

### Using TLS 
In case you want or need encryption if the connection, all you need to do is add the certs as volume to the docker container. 
```
-v /path/to/key:/key.pem -v /path/to/cert:/cert.pem
```
if both files are present at the root of the docker container, the app will automatically switch to TLS. 
