package main

import (
	"log"
	"net/http"
)

// setheroskill toggles the given skill to belong to a hero.
func (cp *callerPackage) setheroskill(w http.ResponseWriter, r *http.Request) {
	hero := r.FormValue("hero")
	skill := r.FormValue("skill")
	log.Println(hero, skill)
	if !validHero(hero, cp.heros) {
		log.Println("unknown hero skill request:", hero, skill)
		return
	}
	change := -1
	for i, s := range cp.skills {
		if s.Skill == skill {
			change = i
			break
		}
	}
	if change == -1 {
		log.Println("unknown skill request:", hero, skill)
		return
	}
	if cp.skills[change].Owner == hero {
		cp.skills[change].Owner = market
	} else {
		cp.skills[change].Owner = hero
	}
	cp.renderherolist()
}

// a big tracker of minor changes, the plus minus of small things
// always updates the complete world.
func (cp *callerPackage) triggerchange(w http.ResponseWriter, r *http.Request) {
	hero := r.FormValue("hero")
	task := r.FormValue("type")
	if !validHero(hero, cp.heros) {
		log.Println("unknown hero request a world change:", hero, task)
		return
	}
	index := -1
	for heroIndex := range cp.heros {
		if cp.heros[heroIndex].Name == hero {
			index = heroIndex
			break
		}
	}
	switch task {
	case "healthup":
		cp.heros[index].Wounds++
	case "healthdown":
		cp.heros[index].Wounds--
	case "fatigueup":
		cp.heros[index].Fatigue++
	case "fatiguedown":
		cp.heros[index].Fatigue--
	case "healthplus":
		cp.heros[index].HealthPotions++
	case "healthminus":
		cp.heros[index].HealthPotions--
	case "healthbuy":
		cp.heros[index].HealthPotions++
		cp.heros[index].Gold -= 50
	case "fatigueplus":
		cp.heros[index].FatiguePotions++
	case "fatigueminus":
		cp.heros[index].FatiguePotions--
	case "fatiguebuy":
		cp.heros[index].FatiguePotions++
		cp.heros[index].Gold -= 50
	case "goldp50":
		cp.heros[index].Gold += 50
	case "goldp100":
		cp.heros[index].Gold += 100
	case "goldm25":
		cp.heros[index].Gold -= 25
	case "goldm100":
		cp.heros[index].Gold -= 100
	case "goldm250":
		cp.heros[index].Gold -= 250
	case "goldm500":
		cp.heros[index].Gold -= 500
	case "goldm750":
		cp.heros[index].Gold -= 750
	case "deselecthero":
		cp.heros[index].Owner = market
		log.Println(cp.heros[index].Name, "takes a vaction from fight evil")
	case "selecthero":
		cp.heros[index].Owner = player
		log.Println(cp.heros[index].Name, "selected as champion to fight evil")
	case "reload":
		log.Println("reload request")
	default:
		log.Println("unknown action request from/for:", hero, "->", task)
		return
	}

	go cp.updateInventory()
	go cp.updateMarket()
	go cp.renderherolist()
}
