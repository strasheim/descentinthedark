$(window).on('load', function () {
  var hash = window.location.hash;
  if (hash) {
    hash = hash.replace('#',''); // strip the # at the beginning of the string
    hash = hash.replaceAll('%20',' '); // there will be spaces
 //   hash = hash.replace(/([^a-z0-9]+)/gi, '-'); // strip all non-alphanumeric characters
    $.ajax({
      url: "glossardata",
      type: "GET",
      data: "info="+hash,
      success: function(templated) {
        $('#glossar').html(templated);
      },
      error: function(response) {
        alert('glossar data load failed'+response);
      },
    });
  }
});
