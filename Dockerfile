## Golang Build Container
FROM registry.gitlab.com/strasheim/go-container AS builder

WORKDIR /go/b
COPY . .
COPY .golangci.yml .golangci.yml
ENV GOARCH=amd64 GOOS=linux CGO_ENABLED=0
RUN go build -ldflags "-s -w -extldflags \"-static\""
RUN golangci-lint run

## Application Container
FROM scratch 

COPY --from=builder /go/b/descentinthedark  /descentinthedark
COPY --from=builder /etc/ssl/certs          /etc/ssl/certs/
COPY --from=builder /go/b/html/             /html/
COPY --from=builder /go/b/*.json            .

USER 65534
EXPOSE 3000
CMD  ["/descentinthedark"]
