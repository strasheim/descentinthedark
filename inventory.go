package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"log"
	"sort"
	"text/template"
)

// updateInventory hands out the rendered inventory table
func (cp *callerPackage) updateInventory() {
	clientjson := &rendered{Mtype: "renderedinventory", Data: inventoryTemplate(cp.items, herosInUse(cp.heros))}
	clientblob, _ := json.Marshal(clientjson)
	cp.hub.broadcast <- clientblob
	log.Println("inventory rendered")
}

// inventoryTemplate reads the template file from disk and calls the one shop
// pre-prep function
func inventoryTemplate(items allItems, heros []string) string {
	tmpl, err := template.ParseFiles("templates/inventory.tmpl")
	if err != nil {
		log.Println(err)
	}
	var buf bytes.Buffer
	err = tmpl.Execute(&buf, sortTheInventory(items, heros))
	if err != nil {
		log.Println("rendering the inventory failed:", err.Error())
		return base64.StdEncoding.EncodeToString([]byte(err.Error()))
	}
	return base64.StdEncoding.EncodeToString(buf.Bytes())
}

// sort the Inventory reduces the list and sorts by hero name
func sortTheInventory(a allItems, heros []string) allItems {
	var inventory allItems
	for _, item := range a {
		if item.Owner == "market" || item.Owner == "discarded" {
			continue
		}
		if item.Itemtype == "Cache" {
			continue
		}
		item.Hands = hands2icons(item.Hands)
		item.Attack = dice2html(item.Attack, "18")
		item.Effect = pipe2newLines(item.Effect, "13")
		item.Details = heros
		item.Cost = ((item.Cost / 25) + 1) / 2 * 25
		inventory = append(inventory, item)
	}
	sort.Slice(inventory, func(i, j int) bool {
		return inventory[i].Owner < inventory[j].Owner
	})
	return inventory
}
