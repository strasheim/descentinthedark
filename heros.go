// nolint -- reads in the struct
package main

import (
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"net/url"
	"strconv"
)

const market = "market"
const player = "player"

type allHeros []struct {
	Name           string `json:"Name"`
	Wounds         int    `json:"Wounds"`
	Fatigue        int    `json:"Fatigue"`
	Armor          int    `json:"Armor"`
	Speed          int    `json:"Speed"`
	Melee          int    `json:"Melee"`
	Ranged         int    `json:"Ranged"`
	Magic          int    `json:"Magic"`
	Fighting       int    `json:"Fighting"`
	Subterfuge     int    `json:"Subterfuge"`
	Wizardry       int    `json:"Wizardry"`
	Conquest       int    `json:"Conquest"`
	Silver         int    `json:"Silver"`
	Gold           int    `json:"Gold"`
	Experience     int    `json:"Experience"`
	Ability        string `json:"Ability"`
	Origin         string `json:"origin"`
	Owner          string `json:"Owner"`
	HealthPotions  int    `json:"HealthPotions"`
	FatiguePotions int    `json:"FatiguePotions"`
	ActiveWeapon   string `json:"ActiveWeapon"`
	ActiveArmor    string `json:"ActiveArmor"`
	ActiveShield   string `json:"ActiveShield"`
}

// herosInUse returns an slice of hero names currently used in the party
func herosInUse(heros allHeros) (inUse []string) {
	for _, h := range heros {
		if h.Owner == player {
			inUse = append(inUse, h.Name)
		}
	}
	return inUse
}

// diceRoll is used by the rendering
type diceRoll struct {
	Dice string
	Roll string
}

type renderDiceRoll struct {
	Row  []diceRoll
	Hero string
}

// heroAttack returns a randam number for each dice a hero has in his attack
func heroAttack(cp *callerPackage, hero string) renderDiceRoll {
	var dr []diceRoll
	for n := range cp.heros {
		if cp.heros[n].Name != hero {
			continue
		}
		for _, item := range cp.items {
			if item.Name != cp.heros[n].ActiveWeapon {
				continue
			}
			for _, char := range item.Attack {
				switch char {
				case 'R':
					dr = append(dr, diceRoll{Dice: "red", Roll: strconv.Itoa(rand.Intn(6) + 1)})
				case 'W':
					dr = append(dr, diceRoll{Dice: "white", Roll: strconv.Itoa(rand.Intn(6) + 1)})
				case 'B':
					dr = append(dr, diceRoll{Dice: "blue", Roll: strconv.Itoa(rand.Intn(6) + 1)})
				case 'G':
					dr = append(dr, diceRoll{Dice: "green", Roll: strconv.Itoa(rand.Intn(6) + 1)})
				case 'Y':
					dr = append(dr, diceRoll{Dice: "yellow", Roll: strconv.Itoa(rand.Intn(6) + 1)})
				case 'S':
					dr = append(dr, diceRoll{Dice: "black", Roll: strconv.Itoa(rand.Intn(6) + 1)})
				case 'X':
					dr = append(dr, diceRoll{Dice: "silver", Roll: strconv.Itoa(rand.Intn(6) + 1)})
				case 'Z':
					dr = append(dr, diceRoll{Dice: "gold", Roll: strconv.Itoa(rand.Intn(6) + 1)})
				}
			}
			power := 0
			switch item.Attack[0:1] {
			case "R":
				power = cp.heros[n].Melee
			case "W":
				power = cp.heros[n].Magic
			case "B":
				power = cp.heros[n].Ranged
			}
			for i := 0; i < power; i++ {
				dr = append(dr, diceRoll{Dice: "black", Roll: strconv.Itoa(rand.Intn(6) + 1)})
			}
			return renderDiceRoll{Row: dr, Hero: hero}
		}
	}
	var noWeapon []diceRoll
	noWeapon = append(noWeapon, diceRoll{Dice: "red", Roll: strconv.Itoa(rand.Intn(6) + 1)})
	return renderDiceRoll{Row: noWeapon, Hero: hero}
}

func drawhero(heros allHeros) string {
	var available []int
	for i, v := range heros {
		if v.Owner == "market" {
			available = append(available, i)
		}
	}
	if len(available) >= 1 {
		n := rand.Int() % len(available)
		n = available[n]
		heros[n].Owner = "drawn"
		return heros[n].Name
	} else {
		return "No more Heros to be found"
	}
}

func validHero(hero string, heros allHeros) bool {
	if len(hero) == 0 {
		return false
	}
	for _, v := range heros {
		if v.Name == hero {
			return true
		}
	}
	hero, _ = url.PathUnescape(hero)
	for _, v := range heros {
		if v.Name == hero {
			return true
		}
	}
	return false
}

func readherojson(file string) allHeros {
	var data allHeros
	content, err := ioutil.ReadFile(file)
	must(err)
	must(json.Unmarshal(content, &data))
	return data
}
