// nolint - reads in the struct
package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
)

type allSkills []struct {
	Skill        string `json:"Skill"`
	Details      string `json:"Details"`
	Skilltype    string `json:"Skilltype"`
	Owner        string `json:"Owner"`
	Origin       string `json:"origin"`
	RoadToLegend string `json:"Road to Legend"`
}

func drawskill(Skilltype string, hero string, skills allSkills) string {
	var available []int
	log.Println("Looking for:", Skilltype, hero, len(skills))
	for i, v := range skills {
		if v.Skilltype == Skilltype && v.Owner == "market" {
			available = append(available, i)
		}
	}
	log.Println("Found possible skills:", len(available))
	if len(available) >= 1 {
		n := rand.Int() % len(available)
		n = available[n]
		log.Println("Took:", skills[n].Skill)
		skills[n].Owner = hero
		return skills[n].Skill
	} else {
		return "Out of Skills"
	}
}

func updateHeroSkill(hero string, SkillName string, skills allSkills) allSkills {
	changling := 100000
	for n, v := range skills {
		if v.Skill == SkillName {
			changling = n
		}
	}
	if changling != 100000 {
		if skills[changling].Owner != "market" {
			skills[changling].Owner = "market"
		} else {
			skills[changling].Owner = hero
		}
	}
	return skills
}

func showHeroSkills(hero string, skills allSkills) []int {
	var list []int
	for i, v := range skills {
		if v.Owner == hero {
			list = append(list, i)
		}
	}
	return list
}

func skillMerger(new allSkills, old allSkills, heros allHeros) {
	// The order should be the same, therefore we can snip the changes in
	log.Println("Importing skills")
	for i, v := range new {
		if v.Owner == "market" || validHero(v.Owner, heros) == true {
			old[i].Owner = v.Owner
		} else {
			log.Println("Invalid owner:", v.Owner)
		}
	}
}

func allNoneMarketSkills(skills allSkills) allSkills {
	var available []int
	var inventory allSkills
	for i, v := range skills {
		if v.Owner != "market" {
			available = append(available, i)
		}
	}
	for _, n := range available {
		inventory = append(inventory, skills[n])
	}
	return inventory
}

func allMarketSkills(skills allSkills) allSkills {
	var available []int
	var inventory allSkills
	for i, v := range skills {
		if v.Owner == "market" {
			available = append(available, i)
		}
	}
	for _, n := range available {
		inventory = append(inventory, skills[n])
	}
	return inventory
}

func readskilljson(file string) allSkills {
	var data allSkills
	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Panic(err, file+" not found\n")
	}
	err = json.Unmarshal(content, &data)
	if err != nil {
		log.Panic(err)
	}
	return data
}
