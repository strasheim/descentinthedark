package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"log"
	"strings"
	"text/template"

	"github.com/jinzhu/copier"
)

type heroView struct {
	Wounds         int
	Fatigue        int
	Armor          int
	Speed          int
	Melee          int
	Ranged         int
	Magic          int
	Gold           int
	FatiguePotions int
	HealthPotions  int
	ActiveWeapon   string
	ActiveArmor    string
	ActiveShield   string
	Ability        string
	Name           string
	Skills         []string
	Equipped       Equipped
	AttackButton   string
}

type Equipped struct {
	ArmorValue   string
	ArmorName    string
	ShieldValue  string
	ShieldName   string
	WeaponImage  string
	WeaponEffect string
	OtherItems   []string
}

// renderherolist is called on any update of hero stats
func (cp *callerPackage) renderherolist() {
	clientjson := &rendered{Mtype: "renderherolist", Data: heroListTemplate(cp)}
	clientblob, _ := json.Marshal(clientjson)
	cp.hub.broadcast <- clientblob
	log.Println("Herolist rendered")
}

// heroListTemplate composes the required structure for the tmpl file and hands back
// base64 encoded HTML to be parsed out
func heroListTemplate(cp *callerPackage) string {
	tmpl, err := template.ParseFiles("templates/herolist.tmpl")
	if err != nil {
		log.Println(err)
	}
	var buf bytes.Buffer
	err = tmpl.Execute(&buf, composeHeroView(cp))
	if err != nil {
		log.Println(err)
	}
	return base64.StdEncoding.EncodeToString(buf.Bytes())
}

// composeHeroView generates a complete view of all active heros
func composeHeroView(cp *callerPackage) []heroView {
	var hv []heroView
	for n := range cp.heros {
		if cp.heros[n].Owner == market {
			continue
		}
		var sh heroView
		err := copier.Copy(&sh, cp.heros[n])
		if err != nil {
			log.Println("failed to copy hero", cp.heros[n].Name, "into the heroview", err)
		}
		for _, skill := range cp.skills {
			if skill.Owner != cp.heros[n].Name {
				continue
			}
			sh.Skills = append(sh.Skills, skill.Skill)
		}

		for _, item := range cp.items {
			if item.Owner == cp.heros[n].Name {
				switch {
				case strings.HasPrefix(item.Itemtype, "Other"):
					sh.Equipped.OtherItems = append(sh.Equipped.OtherItems, item.Name)
				}
			}
			if item.Name == cp.heros[n].ActiveShield {
				sh.Equipped.ShieldName = item.Name
				sh.Equipped.ShieldValue = pipe2newLines(item.Effect, "18")
			}
			if item.Name == cp.heros[n].ActiveArmor {
				sh.Equipped.ArmorName = item.Name
				sh.Equipped.ArmorValue = pipe2newLines(item.Effect, "18")
			}
			if item.Name == cp.heros[n].ActiveWeapon {
				sh.Equipped.WeaponEffect = pipe2newLines(item.Effect, "18")
				view := dice2html(item.Attack, "30")
				var blackdice int
				switch item.Attack[0:1] {
				case "R":
					blackdice = cp.heros[n].Melee
					sh.AttackButton = "Rattack"
				case "W":
					blackdice = cp.heros[n].Magic
					sh.AttackButton = "Wattack"
				case "B":
					blackdice = cp.heros[n].Ranged
					sh.AttackButton = "Battack"
				default:
					blackdice = 0
				}
				for i := 0; i < blackdice; i++ {
					view += "<img src='images/black4.png' width='30'>"
				}
				sh.Equipped.WeaponImage = view
			}
		}
		if sh.Equipped.WeaponImage == "" {
			sh.Equipped.WeaponImage = "<img src='images/red2.png' width='30'>"
			sh.AttackButton = "Rattack"
		}
		hv = append(hv, sh)
	}
	return hv
}
